<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="content">
	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>
	<?php edit_post_link('Edit This Post', '<p>', '</p>'); ?>
</div>
<?php endwhile; else: ?>
<p><strong>No posts were found.</strong></p>
<p>We apologize for any inconvenience, please <a href="<?php bloginfo('url'); ?>/" title="<?php bloginfo('description'); ?>">return to the home page</a>.</p>
<?php endif; ?>

<div id="sidebar" class="sidebars">
	<?php if ( !dynamic_sidebar('Sidebar') ) : ?>
		<!-- Sidebar Here -->
	<?php endif; ?>
</div>

<?php get_footer(); ?>
