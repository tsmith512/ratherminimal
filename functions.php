<?php
add_action( 'widgets_init', 'ratherminimal_add_sidebars' );
function ratherminimal_add_sidebars() {
	register_sidebar(
		array(
			'id' => 'Sidebar',
			'name' => __( 'Sidebar' ),
			'description' => __( 'Sidebar Right of Content' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>'
		)
	);
	register_sidebar(
		array(
			'id' => 'SlimFooter',
			'name' => __( 'SlimFooter' ),
			'description' => __( 'Slim Mid-Footer Area' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>'
		)
	);
	register_sidebar(
		array(
			'id' => 'LargeFooter',
			'name' => __( 'LargeFooter' ),
			'description' => __( 'Large Right Footer Area' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>'
		)
	);
}

function ratherminimal_register_custom_menus() {
	register_nav_menus(
		array(
			'MetaMenuA' => __('Meta Menu'),
			'PrimaryMenu' => __('Primary Menu'),
			'FooterMenuA' => __('Footer Menu A'),
			'FooterMenuB' => __('Footer Menu B'),
		)
	);
}
add_action('init', 'ratherminimal_register_custom_menus');


add_filter( 'the_content', 'ratherminimal_replacements' );
function ratherminimal_replacements ( $content ) {
	$patterns = array();
	$patterns[0] = '/\[\[(.+)\]\]/';
	$replacements = array();
	$replacements[0] = '<span class="placeholder">[[ $1 ]]</span>';
	return preg_replace($patterns, $replacements, $content);
}
?>