	</div><!--/#bodyArea-->
	<div id="footerA">
			<nav id="FooterMenuA"><ul>
			<?php $FooterMenuA_options = array(
			'theme_location'  => 'FooterMenuA',
			'container'       => false, 
			'echo'            => true,
			'fallback_cb'     => false,
			'items_wrap' => '%3$s',
			'depth'           => 1 );
			?>
			<?php wp_nav_menu( $FooterMenuA_options ); ?>
			</ul></nav>

			<nav id="FooterMenuB"><ul>
			<?php $FooterMenuB_options = array(
			'theme_location'  => 'FooterMenuB',
			'container'       => false, 
			'echo'            => true,
			'fallback_cb'     => false,
			'items_wrap' => '%3$s',
			'depth'           => 1 );
			?>
			<?php wp_nav_menu( $FooterMenuB_options ); ?>
			</ul></nav>

		<div id="SlimFooter" class="sidebars">
		    <?php if ( !dynamic_sidebar('SlimFooter') ) : ?>
				<!-- Sidebar Here -->
		    <?php endif; ?>
		</div>

		<div id="LargeFooter" class="sidebars">
		    <?php if ( !dynamic_sidebar('LargeFooter') ) : ?>
				<!-- Sidebar Here -->
		    <?php endif; ?>
		</div>
	</div><!--/#footerA-->
	<footer id="footerB">
		&copy;<?php echo date('Y'); ?> <?php bloginfo('name'); ?>
	</footer><!--/#footer-->
</div><!--/.center-->
<?php wp_footer(); ?>
</body>
</html>