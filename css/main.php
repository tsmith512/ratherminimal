<?php
header('Content-type: text/css');
require('../lib/lessc.inc.php');

try {
	$less = new lessc('main.less');
	$css = $less->parse();
} catch  (exception $ex) {
	exit("lessc fatal error:\n" . $ex->getMessage());
}

echo $css;

?>
