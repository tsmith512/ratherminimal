<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title><?php
if (wp_title('', false) == "" || wp_title('', false) == "Home") { // Page title is blank (i.e. it's the home page)
     echo(get_bloginfo('name'));
} else { // There is a title for this page, use it and follow it with "| Sitename"
     echo(wp_title('', false) . ' | ' . get_bloginfo('name'));
} ?></title>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/main.php" type="text/css" media="screen,projection" />
<?php wp_enqueue_script("jquery"); ?>
<?php wp_head(); ?>
<script src="<?php bloginfo('template_directory'); ?>/js/main.js" type="text/javascript"></script>
<!--[if IE]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script><![endif]-->
</head>
<body>
<div class="center">
	<header>
		<div id="headline">
			<h1><a href="/"><?php bloginfo('name'); ?></a></h1>
			<span id="subhead"><?php bloginfo('description'); ?></span>
		</div>
		<nav><ul>
		<?php $MetaMenuA_options = array(
		'theme_location'  => 'MetaMenuA',
		'container'       => false, 
		'echo'            => true,
		'fallback_cb'     => false,
		'items_wrap' => '%3$s',
		'depth'           => 1 );
		?>
		<?php wp_nav_menu( $MetaMenuA_options ); ?>
		</ul></nav>
	</header>
	<nav id="primaryNav">
		<ul>
		<?php $PrimaryMenu_options = array(
		'theme_location'  => 'PrimaryMenu',
		'container'       => false, 
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'items_wrap' => '%3$s',
		'depth'           => 2 );
		?>
		<?php wp_nav_menu( $PrimaryMenu_options ); ?>
		</ul>
	</nav><!--/#primaryNav-->
	<div id="bodyArea">
		<nav id="secondNav">
			<?php
			if($post->post_parent) {
				$children = wp_list_pages("title_li=&include=".$post->post_parent."&echo=0");
				$children .= wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
			} else {
				$children = wp_list_pages("title_li=&include=".$post->ID."&echo=0");
				$children .= wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
			}
			
			if ($children) { ?>
				<ul>
					<?php echo $children; ?>
				</ul>
			<?php } ?>
		</nav>