jQuery(function($) {
   /* Get the window's width, and check whether it is narrower than 480 pixels */
   
	checkMenu = function() {
		var windowWidth = $(window).width();
		if (windowWidth <= 600 && $('select#primaryMenu').length == 0) {

			/* Clone our navigation */
			var mainNavigation = $('nav#primaryNav').clone();

			/* Replace unordered list with a "select" element to be populated with options, and create a variable to select our new empty option menu */
			$('nav#primaryNav').html('<select id="primaryMenu"></select>');
			var selectMenu = $('select#primaryMenu');

			/* Navigate our nav clone for information needed to populate options */
			$(mainNavigation).children('ul').children('li').each(function() {

				/* Get top-level link and text */
				var href = $(this).children('a').attr('href');
				var text = $(this).children('a').text();

				/* Append this option to our "select" */
				$(selectMenu).append('<option value="'+href+'">'+text+'</option>');

				/* Check for "children" and navigate for more options if they exist */
				if ($(this).children('ul').length > 0) {
					$(this).children('ul').children('li').each(function() {

						/* Get child-level link and text */
						var href2 = $(this).children('a').attr('href');
						var text2 = $(this).children('a').text();

						/* Append this option to our "select" */
						$(selectMenu).append('<option value="'+href2+'">--- '+text2+'</option>');
					});
				}
			});
			
			$(selectMenu).change(function() {
				location = this.options[this.selectedIndex].value;
			});
		}
	};
	
	$(document).ready(function(){ checkMenu(); });
	$(window).resize(function(){ checkMenu(); });
});